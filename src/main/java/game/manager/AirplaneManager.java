package game.manager;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;

import game.GameThread;
import game.gui.GamePlay;
import game.model.Student;
import game.model.Airplane;
import game.utils.PropertiesUtils;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class AirplaneManager extends GameThread implements KeyListener {
	public static final int NO_MISSILES = 40;

	private LinkedList<Airplane> airplanes = new LinkedList<>();
	private GamePlay gamePlay;
	private int missiles = NO_MISSILES;

	public AirplaneManager(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (gamePlay.getGameState() == GamePlay.GameState.GAME_PLAY) {
			if (hasMissiles()) {
				Student student = gamePlay.getStudent();
				int key = e.getKeyCode();

				if (key == KeyEvent.VK_SPACE) {
					Airplane airplane = new Airplane(student.getX(), student.getY(), gamePlay);
					airplanes.add(airplane);
					missiles--;
				}
			} else {
				if(gamePlay.getStudent().getScore() > gamePlay.getHighScore()){
					gamePlay.setHighScore(gamePlay.getStudent().getScore());
					
					PropertiesUtils.writeHighScore(gamePlay.getHighScore());
				}
				gamePlay.setKiller(null);
				gamePlay.setGameState(GamePlay.GameState.GAME_OVER);
			}
		}
	}

	private boolean hasMissiles() {
		return running && missiles > 0;
	}

	public void check() {
		for (int i = 0; i < airplanes.size(); i++) {
			Airplane airplane = airplanes.get(i);
			if (airplane.getY() < 0)
				airplanes.remove(airplane);
			airplane.check();
		}
	}

	public void show(Graphics g) {
		for (Airplane airplane : airplanes) {
			airplane.show(g);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent e) { }

	public LinkedList<Airplane> getAirplanes() {
		return airplanes;
	}

	public int getMissiles() {
		return missiles;
	}

	public void setMissiles(int missiles) {
		this.missiles = missiles;
	}

	public void addMissiles(int m) {
		this.missiles += m;
	}

}
