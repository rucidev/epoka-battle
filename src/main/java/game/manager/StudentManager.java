package game.manager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import game.GameThread;
import game.gui.GamePlay;
import game.model.Student;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class StudentManager extends GameThread implements KeyListener{
	private GamePlay gamePlay;
	
	public StudentManager(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(running){
			Student student = gamePlay.getStudent();
			int key = e.getKeyCode();
			
			if(key == KeyEvent.VK_LEFT){
				if(student.getX() > 0){
					student.setX(student.getX() - 20);
				}
			}else if(key == KeyEvent.VK_RIGHT){
				if(student.getX() < gamePlay.getCanvasWidth() - 65){
					student.setX(student.getX() + 20);
				}
			}
		}
		
	}

	@Override
	public void keyTyped(KeyEvent paramKeyEvent) { }

	@Override
	public void keyReleased(KeyEvent paramKeyEvent) { }
}
