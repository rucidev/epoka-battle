package game.manager;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class StartMenuManager implements KeyListener{
	private GamePlay gamePlay;
	
	public StartMenuManager(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}
	
	public void show(Graphics g){
		Font font = new Font("courier new", Font.BOLD, 20);
		
		g.setFont(font);
		g.setColor(Color.YELLOW);
		g.drawString("PRESS SPACE TO START", 270, 500);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(gamePlay.getGameState() == GamePlay.GameState.START_MENU){
			if(e.getKeyCode() == KeyEvent.VK_SPACE){
				gamePlay.setGameState(GamePlay.GameState.GAME_PLAY);
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent paramKeyEvent) { }
}
