package game;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class GameThread implements Runnable{
    protected Thread thread;
    protected boolean running = false;

    @Override
    public void run() {
        while (running) {

        }
        stop();
    }

    public synchronized void start(String name){
        if(running)
            return;
        running = true;
        thread = new Thread(this);
        thread.setName(name);
        thread.start();
    }

    public synchronized void stop() {
        if (!running)
            return;

        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
