package game.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class PropertiesUtils {
	private static final String GAME_FILE = "game.properties";

	public static int readGameData() {
		int highscore = 0;
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File(GAME_FILE)));
			highscore = Integer.parseInt(properties.getProperty("highscore"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return highscore;
	}

	public static void writeHighScore(int score) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File(GAME_FILE)));
			properties.setProperty("highscore", String.valueOf(score));
			
			FileOutputStream fileOutputStream = new FileOutputStream(GAME_FILE);
			properties.save(fileOutputStream, null);
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
