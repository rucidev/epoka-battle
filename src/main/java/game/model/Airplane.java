package game.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;

import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class Airplane extends GameWindow {

	public Airplane(int x, int y, GamePlay gamePlay) {
		super(x, y, gamePlay);
		try {
			this.setImage(getGamePlay().getImageReader().loadImage("/airplane.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void check(){
		setY(getY() - 15);
	}

	@Override
	public void show(Graphics g) {
		g.drawImage(getImage(), getX()+18, getY(), 30, 30,  null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(getX(), getY(), 30, 30);
	}
}
