package game.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;

import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class Collision extends GameWindow {

	public Collision(int x, int y, GamePlay gamePlay) {
		super(x, y, gamePlay);
		try {
			this.setImage(getGamePlay().getImageReader().loadImage("/boom.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void show(Graphics g) {
		g.drawImage(getImage(), getX(), getY(), 100, 100,  null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(getX(), getY(), 100, 100);
	}
}
