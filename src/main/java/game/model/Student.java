package game.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;

import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class Student extends GameWindow {
    public static final int ALEKS = 1;
    public static final int GRENT = 2;
    public static final int PATRIK = 3;
    public static final int REGAN = 4;


    private int score = 0;

    public Student(int x, int y, GamePlay gamePlay, int playerType) {
        super(x, y, gamePlay);
        try {
            System.out.println(playerType);
            if (playerType == ALEKS) {
                this.setImage(getGamePlay().getImageReader().loadImage("/aleks.png"));
            } else if (playerType == GRENT) {
                this.setImage(getGamePlay().getImageReader().loadImage("/grent.png"));
            } else if (playerType == PATRIK) {
                this.setImage(getGamePlay().getImageReader().loadImage("/patrik.png"));
            } else if (playerType == REGAN) {
                this.setImage(getGamePlay().getImageReader().loadImage("/regan.png"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show(Graphics g) {
        g.drawImage(getImage(), getX(), getY(), 64, 64, null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(getX(), getY(), 64, 64);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void addScore(int i) {
        this.score += i;
    }
}
