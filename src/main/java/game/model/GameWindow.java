package game.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import game.GameThread;
import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public abstract class GameWindow extends GameThread {
	private int x;
	private int y;
	private GamePlay gamePlay;
	private BufferedImage image;
	
	public GameWindow(int x, int y, GamePlay gamePlay) {
		super();
		this.x = x;
		this.y = y;
		this.gamePlay = gamePlay;
	}

	public void show(Graphics g){}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public GamePlay getGamePlay() {
		return gamePlay;
	}

	public void setGamePlay(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	public abstract Rectangle getBounds();
}
