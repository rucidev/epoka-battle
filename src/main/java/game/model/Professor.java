package game.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;

import game.gui.GamePlay;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class Professor extends GameWindow {
    private String name;
    private int motion;
    private int velocity = 4;

    public Professor(int x, int y, GamePlay gamePlay, int type, int motion) {
        super(x, y, gamePlay);
        try {
            if (type == 0) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-erind-bedalli.png"));
                this.setName("Professor Erind Bedalli");
            } else if (type == 1) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-enea-mancellari.png"));
                this.setName("Professor Enea Mancellari");
            } else if (type == 2) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-arban-uka.png"));
                this.setName("Professor Arban Uka");
            } else if (type == 3) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-ali-osman.png"));
                this.setName("Professor Ali Osman");
            } else if (type == 4) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-igli-hakrama.png"));
                this.setName("Professor Igli Hakrama");
            } else if (type == 5) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-julian-hoxha.png"));
                this.setName("Professor Julian Hoxha");
            } else if (type == 6) {
                this.setImage(getGamePlay().getImageReader().loadImage("/professor-dimitri-karras.png"));
                this.setName("Professor Dimitri Karras");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.motion = motion;
    }

    public void down() {
        setY(getY() + velocity);
        checkDirection();
        setMotion();
    }

    private void checkDirection() {
        if (motion == 0) {
            left();
        } else {
            right();
        }
    }

    private void setMotion() {
        if (getX() < 1) {
            motion = 1;
        }
        if (getX() > getGamePlay().getCanvasWidth() - 65) {
            motion = 0;
        }
    }

    public void left() {
        setX(getX() - velocity);
    }

    public void right() {
        setX(getX() + velocity);
    }

    @Override
    public void show(Graphics g) {
        g.drawImage(getImage(), getX(), getY(), 70, 70, null);
    }

        @Override
        public Rectangle getBounds() {
            return new Rectangle(getX(), getY(), 70, 70);
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
