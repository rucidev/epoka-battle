package game;

import game.gui.StartMenu;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class App {

	public static void main(String[] args) {
		StartMenu startMenu = new StartMenu();
		startMenu.start();
	}
}
