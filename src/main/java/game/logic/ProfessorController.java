package game.logic;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

import game.GameThread;
import game.gui.GamePlay;
import game.model.Professor;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class ProfessorController extends GameThread implements Runnable {
    private LinkedList<Professor> professors = new LinkedList<>();
    private GamePlay gamePlay;
    private int lastX = 0;

    public ProfessorController(GamePlay gamePlay) {
        this.gamePlay = gamePlay;
        throwManyProfessors();
    }

    public void check() {
        for (int i = 0; i < professors.size(); i++) {
            Professor professor = professors.get(i);
            if (professor.getY() > gamePlay.getCanvasHeight()) {
                professor.stop();
                professors.remove(professor);
            }
            professor.down();
        }

        if (professors.size() < 5) {
            throwNewProfessors();
        }
    }

    private void throwManyProfessors() {
        for (int x = 0; x < gamePlay.getCanvasWidth() - 65; x += 100) {
            int professorType = ThreadLocalRandom.current().nextInt(0, 7);
            int motion = ThreadLocalRandom.current().nextInt(0, 2);
            Professor newProfessor = new Professor(x, 0, gamePlay, professorType, motion);
            professors.add(newProfessor);
        }
    }

    private void throwNewProfessors() {
        int professorType = ThreadLocalRandom.current().nextInt(0, 7);
        int motion = ThreadLocalRandom.current().nextInt(0, 2);
        int startPosition = ThreadLocalRandom.current().nextInt(0, gamePlay.getCanvasWidth() - 65);

        if (startPosition < (lastX + 65)) {
            startPosition = lastX - startPosition;
        } else if (startPosition > (lastX - 65)) {
            startPosition = lastX + startPosition;
        }
        if (startPosition < 0) {
            startPosition = 0;
        } else if (startPosition > gamePlay.getCanvasWidth() - 65) {
            startPosition = gamePlay.getCanvasWidth() - 65;
        }
        Professor newProfessor = new Professor(startPosition, 0, gamePlay, professorType, motion);
        professors.add(newProfessor);
        lastX = startPosition + 65;
    }

    public void show(Graphics g) {
        for (int i = 0; i < professors.size(); i++) {
            professors.get(i).show(g);
        }
    }

    public LinkedList<Professor> getProfessors() {
        return professors;
    }

}
