package game.logic;

import java.util.LinkedList;

import game.GameThread;
import game.gui.GamePlay;
import game.model.Airplane;
import game.model.Professor;
import game.model.Collision;
import game.utils.PropertiesUtils;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class CollisionController extends GameThread implements Runnable {
	private LinkedList<Professor> professors;
	private LinkedList<Airplane> airplanes;

	private GamePlay gamePlay;

	public CollisionController(LinkedList<Professor> professors, LinkedList<Airplane> airplanes, GamePlay gamePlay) {
		super();
		this.professors = professors;
		this.airplanes = airplanes;
		this.gamePlay = gamePlay;
	}

	public void checkCollisions() {
		if (running) {
			synchronized (this) {
				for (int i = 0; i < professors.size(); i++) {
					Professor professor = professors.get(i);

					for (int j = 0; j < airplanes.size(); j++) {
						Airplane airplane = airplanes.get(j);
                        checkAirplaneCollision(professor, airplane);
                    }
                    checkStudentCollision(professor);
                }
			}
		}
	}

    private void checkStudentCollision(Professor professor) {
        if (professor.getBounds().intersects(gamePlay.getStudent().getBounds())) {
            Collision collision = new Collision(professor.getX(), professor.getY(), gamePlay);
            collision.show(gamePlay.getGraphics());

            professors.remove(professor);

            if (gamePlay.getStudent().getScore() > gamePlay.getHighScore()) {
                gamePlay.setHighScore(gamePlay.getStudent().getScore());

                PropertiesUtils.writeHighScore(gamePlay.getHighScore());
            }
            gamePlay.setKiller(professor);
            gamePlay.setGameState(GamePlay.GameState.GAME_OVER);
        }
    }

    private void checkAirplaneCollision(Professor professor, Airplane airplane) {
        if (professor.getBounds().intersects(airplane.getBounds())) {
            Collision collision = new Collision(professor.getX(), professor.getY(), gamePlay);
            collision.show(gamePlay.getGraphics());

            professor.stop();
            professors.remove(professor);
            airplanes.remove(airplane);

            gamePlay.getAirplaneManager().addMissiles(3);

            gamePlay.getStudent().addScore(10);
        }
    }

    @Override
	public void run() {
		while (running) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stop();
	}
}
