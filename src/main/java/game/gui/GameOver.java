package game.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static game.manager.AirplaneManager.NO_MISSILES;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class GameOver implements KeyListener{
	private GamePlay gamePlay;

	public GameOver(GamePlay gamePlay) {
		this.gamePlay = gamePlay;
	}

	public void show(Graphics g){
		Font font = new Font("courier new", Font.BOLD, 20);

		g.setFont(font);
		g.setColor(Color.RED);
		g.drawString("GAME OVER", 340, 50);


		if(gamePlay.getKiller() != null) {
			g.setColor(Color.GREEN);
			g.drawString(gamePlay.getKiller().getName(), 290, 80);

			g.setColor(Color.RED);
			g.drawString("KILLED YOU", 290, 100);
		}


		g.setColor(Color.GREEN);
		g.drawString("PROFESSORS KILLED", 290, 120);

		g.setColor(Color.WHITE);
		g.drawString(String.valueOf(gamePlay.getStudent().getScore()/10), 290, 140);

		g.setColor(Color.GREEN);
		g.drawString("SCORE", 290, 160);

		g.setColor(Color.WHITE);
		g.drawString(String.valueOf(gamePlay.getStudent().getScore()), 290, 180);

		g.setColor(Color.YELLOW);
		g.drawString("PRESS ENTER TO START", 290, 500);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(gamePlay.getGameState() == GamePlay.GameState.GAME_OVER){
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				gamePlay.setGameState(GamePlay.GameState.GAME_PLAY);
				gamePlay.getAirplaneManager().setMissiles(NO_MISSILES);
				gamePlay.getStudent().setScore(0);
				gamePlay.getProfessorController().getProfessors().clear();
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent paramKeyEvent) { }
}
