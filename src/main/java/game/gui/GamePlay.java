package game.gui;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.*;

import game.GameThread;
import game.logic.CollisionController;
import game.logic.ProfessorController;
import game.manager.StartMenuManager;
import game.manager.StudentManager;
import game.manager.AirplaneManager;
import game.model.Professor;
import game.model.Student;
import game.utils.ImageReader;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class GamePlay extends GameThread implements Runnable {
    public static final String GAME_TITLE = "Epoka Battle";
    private int highScore = 0;

    private static final int WIDTH = 400;
    private static final int HEIGHT = (WIDTH / 12) * 9;
    private static final int SCALE = 2;

    JFrame jFrame = new JFrame(GAME_TITLE);
    private Canvas canvas = new Canvas();
    private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    private ImageReader imageReader = new ImageReader();

    private Student student;
    private StudentManager studentManager;
    private AirplaneManager airplaneManager;
    private ProfessorController professorController;
    private CollisionController collisionController;

    private StartMenuManager startMenuManager;
    private GameData gameData;
    private GameOver gameOver;
    private Professor killer;

    public enum GameState {
        START_MENU, GAME_PLAY, GAME_OVER
    }

    private GameState gameState = GameState.START_MENU;

    public GamePlay(int playerType) {
        Dimension dimension = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
        canvas.setSize(dimension);
        canvas.setPreferredSize(dimension);
        canvas.setMaximumSize(dimension);

        jFrame.add(canvas);
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setVisible(true);
        jFrame.setResizable(false);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        studentManager = new StudentManager(this);
        jFrame.addKeyListener(studentManager);

        airplaneManager = new AirplaneManager(this);
        jFrame.addKeyListener(airplaneManager);

        professorController = new ProfessorController(this);
        collisionController = new CollisionController(professorController.getProfessors(), airplaneManager.getAirplanes(), this);

        student = new Student(400, canvas.getHeight() - 100, this, playerType);

        startMenuManager = new StartMenuManager(this);
        jFrame.addKeyListener(startMenuManager);

        gameData = new GameData(this);

        gameOver = new GameOver(this);
        jFrame.addKeyListener(gameOver);
    }

    @Override
    public void run() {
        while (running) {
            show();
            check();
        }
        stop();
    }

    private void show() {
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();
        if (bufferStrategy == null) {
            canvas.createBufferStrategy(3);
            return;
        }

        Graphics g = bufferStrategy.getDrawGraphics();
        g.drawImage(image, 0, 0, canvas.getWidth(), canvas.getHeight(), canvas);

        try {
            BufferedImage background = imageReader.loadImage("/epoka-bg.jpg");
            g.drawImage(background, 0, 0, canvas.getWidth(), canvas.getHeight(), canvas);

            if (gameState == GameState.START_MENU) {
                showStartMenu(g);
            } else if (gameState == GameState.GAME_PLAY) {
                showGamePlay(g);
            } else if (gameState == GameState.GAME_OVER) {
                gameOver.show(g);
            }
        } catch (IOException e) {
            System.out.println("Closed.");
        }

        g.dispose();
        bufferStrategy.show();
    }

    private void showGamePlay(Graphics g) {
        student.show(g);
        airplaneManager.show(g);
        professorController.show(g);
        gameData.show(g);
    }

    private void showStartMenu(Graphics g) {
        startMenuManager.show(g);
    }

    private void check() {
        if (gameState == GameState.GAME_PLAY) {
            airplaneManager.check();
            professorController.check();
            collisionController.checkCollisions();
        }
    }


    public synchronized void start() {
        super.start("game-thread");
        student.setScore(0);
        studentManager.start("student-thread");
        airplaneManager.start("airplane-thread");
        professorController.start("professor-thread");
        collisionController.start("collision-thread");
    }

    @Override
    public synchronized void stop() {
        super.stop();
        System.exit(1);
    }

    public Graphics getGraphics() {
        return canvas.getGraphics();
    }

    public int getCanvasWidth() {
        return canvas.getWidth();
    }

    public int getCanvasHeight() {
        return canvas.getHeight();
    }

    public ImageReader getImageReader() {
        return imageReader;
    }

    public Student getStudent() {
        return student;
    }

    public AirplaneManager getAirplaneManager() {
        return airplaneManager;
    }

    public ProfessorController getProfessorController() {
        return professorController;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public Professor getKiller() {
        return killer;
    }

    public void setKiller(Professor killer) {
        this.killer = killer;
    }

}
