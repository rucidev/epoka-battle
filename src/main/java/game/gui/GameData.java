package game.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 * @author aleksruci on 15/Feb/2019
 */
public class GameData {
    private GamePlay gamePlay;

    public GameData(GamePlay gamePlay) {
        this.gamePlay = gamePlay;
    }

    public void show(Graphics g) {
        Font font = new Font("courier new", Font.BOLD, 20);

        g.setFont(font);
        showText(g, Color.RED, "HIGH SCORE",  gamePlay.getHighScore(), 20, 40);
        showText(g, Color.RED, "CURRENT SCORE", gamePlay.getStudent().getScore(), 60, 80);
        showText(g, Color.YELLOW, "KILLS", gamePlay.getStudent().getScore() / 10, gamePlay.getCanvasHeight() - 80, gamePlay.getCanvasHeight() - 60);
        showText(g, Color.GREEN, "AIRPLANES", gamePlay.getAirplaneManager().getMissiles(), gamePlay.getCanvasHeight() - 40, gamePlay.getCanvasHeight() - 20);
    }

    private void showText(Graphics g, Color color, String text, int value, int y, int y2) {
        g.setColor(color);
        g.drawString(text, 10, y);

        g.setColor(Color.WHITE);
        g.drawString(String.valueOf(value), 10, y2);
    }
}
