package game.gui;

import game.model.Student;
import game.utils.PropertiesUtils;

import javax.swing.*;
import java.awt.*;

import static game.gui.GamePlay.GAME_TITLE;

/**
 * @author aleksruci on 16/Feb/2019
 */
public class StartMenu {
    private int playerType;

    public StartMenu() {

    }

    public void start() {
        Dimension dimension = new Dimension(400, 400);

        JFrame jFrame = new JFrame(GAME_TITLE);
        jFrame.setSize(dimension);
        jFrame.setPreferredSize(dimension);
        jFrame.setMaximumSize(dimension);
        jFrame.setVisible(true);
        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setResizable(false);

        JToggleButton button1 = getButton(jFrame, "Aleks", Student.ALEKS);
        button1.setBackground(Color.PINK);
        JToggleButton button2 = getButton(jFrame, "Grent", Student.GRENT);
        button2.setBackground(Color.cyan);
        JToggleButton button3 = getButton(jFrame, "Patrik", Student.PATRIK);
        button3.setBackground(Color.GREEN);
        JToggleButton button4 = getButton(jFrame, "Regan", Student.REGAN);
        button4.setBackground(Color.ORANGE);


        JPanel content = new JPanel(new GridLayout(2,0));
        content.add(button1);
        content.add(button2);
        content.add(button3);
        content.add(button4);

        jFrame.add(content);
    }


    private JToggleButton getButton(JFrame jFrame, String name, int playerType) {
        JToggleButton button4 = new JToggleButton(name);
        button4.addActionListener(e -> {
            JToggleButton btn = (JToggleButton) e.getSource();
            if (btn.isSelected()) {
                jFrame.setVisible(false);
                GamePlay gamePlay = new GamePlay(playerType);
                gamePlay.setHighScore(PropertiesUtils.readGameData());
                gamePlay.start();
            }
        });
        return button4;
    }
}
